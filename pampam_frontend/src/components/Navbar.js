import Link from "next/link";
import { Menu, Image } from "semantic-ui-react";
export function Navbar() {
  return (
    <Menu secondary>
      <Menu.Item>
        <Image src="Logo.png" size="mini" rounded />
      </Menu.Item>
      <Menu.Item name="Home" />

      <Menu.Menu position="right">
        <Link as="/" href="/login">
          <Menu.Item name="Login" />
        </Link>
      </Menu.Menu>
    </Menu>
  );
}
