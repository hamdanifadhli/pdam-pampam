import { Grid, Image } from "semantic-ui-react";

export function Hero() {
  return (
    <Grid columns={2}>
      <Grid.Row
        style={{
          height: "80vh",
        }}
      >
        <Grid.Column
          style={{
            marginTop: "auto",
            marginBottom: "auto",
          }}
        >
          <Image src="./img/landing_01.jpg" fluid />
        </Grid.Column>
        <Grid.Column
          style={{
            marginTop: "auto",
            marginBottom: "auto",
          }}
        >
          <h1 style={{ marginBottom: "0" }}>PDAM Automatic Management PAM</h1>
          <h3 style={{ marginTop: "1rem" }}>
            kami memonitoring pam yang keluar masuk dari rumah kalian dan juga
            dari seluruh pipa yang ada di jawa barat
          </h3>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
