import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
} from "semantic-ui-react";
import Link from "next/link";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { API_BASE_URL, MERGE_ID, USER_ID } from "../api/default";

export function Login() {
  const Router = useRouter();
  const [state, setstate] = useState({
    username: "",
    password: "",
    successMessage: null,
  });
  const handleChange = (e) => {
    const { id, value } = e.target;
    setstate((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };

  const loginkeserver = () => {
    // alert(`KECE MANG ${state.username} - ${state.password}`);
    if (state.username && state.password) {
      {
        const payload = {
          username: state.username,
          password: state.password,
        };
        const header = {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        };
        axios
          .post(API_BASE_URL + "user/login", payload, header)
          .then(function (response) {
            if (response.status === 200) {
              //simpen bebass
              console.log(response);
              const payload = {};
              const header = {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
              };
              axios
                .get(
                  API_BASE_URL + "user/pointer/" + response.data.id,
                  payload,
                  header
                )
                .then(function (r) {
                  console.log(r.data[0].Merge_id);
                  localStorage.setItem(MERGE_ID, r.data[0].Merge_id);
                  Router.push("/dashboard/" + r.data[0].Merge_id);
                })
                .catch(function (e) {
                  console.log(e);
                });
              localStorage.setItem(USER_ID, response.data.id);
            } else {
              Router.push("/login");
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    loginkeserver();
  };

  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header
          as="h1"
          style={{ fontSize: "75px" }}
          color="blue"
          textAlign="center"
        >
          <Image src="/logo.png" />
        </Header>
        <Form size="large" onSubmit={handleSubmit} id="loginform">
          <Segment>
            <Form.Input
              fluid
              icon="user"
              id="username"
              name="username"
              value={state.username}
              onChange={handleChange}
              iconPosition="left"
              placeholder="Username"
              required
            />
            <Form.Input
              fluid
              icon="lock"
              id="password"
              name="password"
              value={state.password}
              onChange={handleChange}
              iconPosition="left"
              placeholder="Password"
              type="password"
              required
            />

            <Link as="/home" href="/home">
              <Button color="blue" fluid size="large" type="submit">
                <h1>Login</h1>
              </Button>
            </Link>
          </Segment>
        </Form>
        <Message>
          New Devices? <a href="#">Sign Up</a>
        </Message>
      </Grid.Column>
    </Grid>
  );
}
