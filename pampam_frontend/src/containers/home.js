import {
  Container,
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
  Menu,
  Dropdown,
  Input,
  Card,
} from "semantic-ui-react";
import Link from "next/link";
import { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { API_BASE_URL, MERGE_ID, USER_ID } from "../api/default";

export function Home() {
  const [homeState, setHomeState] = useState([
    {
      id: 1,
      name: "Pampam 01 01",
      slug: "pampam_01_01",
      adress: "Jalan Kenangan no.1",
    },
    {
      id: 2,
      name: "Pampam 01 02",
      slug: "pampam_01_02",
      adress: "Jalan Kenangan no.2",
    },
    {
      id: 3,
      name: "Pampam 01 03",
      slug: "pampam_01_03",
      adress: "Jalan Kenangan no.3",
    },
    {
      id: 4,
      name: "Pampam 02 01",
      slug: "pampam_02_01",
      adress: "Jalan Halal no.1",
    },
  ]);

  return (
    <div>
      <Container>
        <Grid columns="equal">
          <Grid.Column>
            <Image
              style={{
                width: "70%",
                filter: "grayscale(100%)",
              }}
              src="/Logo_ds.png"
              rounded
            />
            <Menu secondary pointing vertical>
              <Link as="/dashboard/pampam_01_01" href="/dashboard/pampam_01_01">
                <Menu.Item name="Dashboard" active />
              </Link>
              <Menu.Item name="Analitics" />
              <Link href="/payment">
                <Menu.Item name="Payments" />
              </Link>
              <Dropdown item text="Settings">
                <Dropdown.Menu>
                  <Dropdown.Header>User Settings</Dropdown.Header>
                  <Dropdown.Item>Device</Dropdown.Item>
                  <Link href="/settings/user/pampam_01_01">
                    <Dropdown.Item>User</Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
          </Grid.Column>
          <Grid.Column
            width={12}
            style={{
              marginTop: "20px",
            }}
          >
            <Menu secondary>
              <Menu.Item name="home" />
              <Menu.Item name="messages" />
              <Menu.Item name="friends" />
              <Menu.Menu position="right">
                <Menu.Item>
                  <Input icon="search" placeholder="Search..." />
                </Menu.Item>
                <Menu.Item name="logout" />
              </Menu.Menu>
            </Menu>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  <Card.Group itemsPerRow={3}>
                    {homeState.map((device) => {
                      return (
                        <Card key={device.id}>
                          <Card.Content>
                            <Image floated="right" size="mini" />
                            <Card.Header>{device.name}</Card.Header>
                            <Card.Meta>{device.slug}</Card.Meta>
                            <Card.Description>
                              <strong>Lokasi:</strong> {device.adress}
                            </Card.Description>
                          </Card.Content>
                          <Card.Content extra>
                            <div className="button">
                              <Link href={"/dashboard/" + device.slug}>
                                <Button fluid basic color="green">
                                  Detil
                                </Button>
                              </Link>
                            </div>
                          </Card.Content>
                        </Card>
                      );
                    })}
                  </Card.Group>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
}
