import React, { Component } from "react";
import { Navbar } from "../components/Navbar";
import { Input, Menu, Container, Image } from "semantic-ui-react";
import { Hero } from "../components/Landing/Hero";

export function Landing() {
  return (
    <Container>
      <Navbar />
      <Hero />
    </Container>
  );
}
