import { useRouter } from "next/router";
import Link from "next/link";
import {
  Grid,
  Segment,
  Menu,
  Dropdown,
  Container,
  Image,
  Input,
  Card,
  GridRow,
  GridColumn,
  List,
  Icon,
  Button,
} from "semantic-ui-react";
import { useState, useEffect } from "react";
import axios from "axios";
import { AreaChart, Area, Tooltip, XAxis, YAxis } from "recharts";
import { API_BASE_URL, MERGE_ID, USER_ID } from "../../api/default";
import ReactMapGL, { Marker } from "react-map-gl";
import React from "react";

const useInterval = (callback, delay) => {
  const savedCallback = React.useRef();

  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  React.useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};

export function Dashboard({
  volume_harian,
  volume_bulanan,
  pengurang_b,
  init_day,
  x,
  vale,
}) {
  const router = useRouter();

  function volvo() {
    axios.put(
      API_BASE_URL + "api/device/pampam_01_01",
      { merge_id: "pampam_01_01", valve_status: state.valve == 0 ? 1 : 0 },
      header
    );
    let x = 0;
    if (state.valve == 0) {
      x = 1;
    } else {
      x = 0;
    }
    setState((prevState) => ({
      ...prevState,
      valve: x,
    }));
  }

  function hahu() {
    axios
      .get(API_BASE_URL + "api/device/baterai/pampam_01_01", payload, header)
      .then(function (response) {
        if (response.status === 200) {
          console.log(response.data);
          setState((prevState) => ({
            ...prevState,
            bate: response.data,
          }));
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const are = [];
  const aru = [];
  let datahehe = new Array(new Object());

  const [state, setState] = useState({
    data_waktu: are,
    data_debit: aru,
    data_kumulatif: 0,
    response_lama: "",
    data: datahehe,
    pengurang_b: pengurang_b,
    x: x,
    bate: 0,
    valve: vale,
    viewport: {
      width: 800,
      height: 175,
      latitude: -6.883001,
      longitude: 107.610348,
      zoom: 16,
    },
  });

  const [position, setPosition] = useState({
    latitude: -6.883001,
    longitude: 107.610348,
  });

  const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

  const SIZE = 20;

  const [seconds, setSeconds] = useState(1);
  useEffect(() => {
    const timer = setInterval(() => {
      setSeconds(seconds + 1);
    }, 100);
    return () => clearInterval(timer);
  });
  const TOKEN =
    "pk.eyJ1IjoiaGFtZGFuaWZhZGhsaSIsImEiOiJja2didmY2MTgwOG1kMnJtd3dkdWNuNzZqIn0.DmrjhvfFsOwT5GF5hH5Q0A";
  // const TOKEN = '';
  const payload = {};
  const header = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };

  axios
    .get(API_BASE_URL + "volume/last/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        var x = "";
        var y = 0;
        x = response.data.Waktu;
        y = response.data.Debit;
        if (response.data.Debit == 0.0) {
          response.data.Debit = 0;
        }
        var a = state.data_waktu.concat(x);
        var b = state.data_debit.concat(y);

        var i;
        var datakosong = [
          {
            waktu: "",
            debit: 0,
          },
        ];
        for (i = 0; i < a.length; i++) {
          let ham = {
            waktu: a[i],
            debit: b[i],
          };
          datakosong.push(ham);
        }

        if (response.data.Waktu !== state.response_lama) {
          setState((prevState) => ({
            ...prevState,
            data_kumulatif: state.data_kumulatif + response.data.Debit,
            data_debit: b,
            data_waktu: a,
            data: datakosong,
            response_lama: response.data.Waktu,
          }));
          hahu();
        }
      } else {
        console.log("eweuh");
      }
    })
    .catch(function (error) {
      console.log(error);
    });

  return (
    <div>
      <Container>
        <Grid columns="equal">
          <Grid.Column>
            <Image
              style={{
                width: "70%",
                filter: "grayscale(100%)",
              }}
              src="/Logo_ds.png"
              rounded
            />
            <Menu secondary pointing vertical>
              <Link as="/dashboard/pampam_01_01" href="/dashboard/pampam_01_01">
                <Menu.Item name="Dashboard" active />
              </Link>
              <Menu.Item name="Analitics" />
              <Link href="/payment">
                <Menu.Item name="Payments" />
              </Link>
              <Dropdown item text="Settings">
                <Dropdown.Menu>
                  <Dropdown.Header>User Settings</Dropdown.Header>
                  <Dropdown.Item>Device</Dropdown.Item>
                  <Link href="/settings/user/pampam_01_01">
                    <Dropdown.Item>User</Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
          </Grid.Column>
          <Grid.Column
            width={12}
            style={{
              marginTop: "20px",
            }}
          >
            <Menu secondary>
              <Menu.Item name="home" />
              <Menu.Item name="messages" />
              <Menu.Item name="friends" />
              <Menu.Menu position="right">
                <Menu.Item>
                  <Input icon="search" placeholder="Search..." />
                </Menu.Item>
                <Menu.Item name="logout" />
              </Menu.Menu>
            </Menu>
            <Segment padded="medium">
              <Grid columns="equal">
                <Grid.Row>
                  <Grid.Column>Welcome {router.query.id}</Grid.Column>
                </Grid.Row>
                <GridRow>
                  <GridColumn>
                    <ReactMapGL
                      {...state.viewport}
                      onViewportChange={(viewport) => setState({ viewport })}
                      mapboxApiAccessToken={TOKEN}
                    >
                      <Marker
                        longitude={position.longitude}
                        latitude={position.latitude}
                      >
                        <svg
                          height={SIZE}
                          viewBox="0 0 24 24"
                          style={{
                            cursor: "pointer",
                            fill: "#d00",
                            stroke: "none",
                            transform: `translate(${-SIZE / 2}px,${-SIZE}px)`,
                          }}
                        >
                          <path d={ICON} />
                        </svg>
                      </Marker>
                    </ReactMapGL>
                  </GridColumn>
                </GridRow>
                <GridRow>
                  <GridColumn>
                    <List style={{ fontSize: "1rem" }}>
                      <List.Item>
                        <List.Icon name="mobile alternate" />
                        <List.Content>Device Name : Pampam_01_01</List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name="microchip" />
                        <List.Content>Device ID : 01</List.Content>
                      </List.Item>
                    </List>
                  </GridColumn>
                  <GridColumn>
                    <List style={{ fontSize: "1rem" }}>
                      <List.Item>
                        <List.Icon name="rss" />
                        <List.Content>Gateway ID : 01</List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name="marker" />
                        <List.Content>
                          Adress : Jalan Sangkuriang no.11, Kelurahan Coblong,
                          Kelurahan Dago, Kota Bandung
                        </List.Content>
                      </List.Item>
                    </List>
                  </GridColumn>
                </GridRow>
                <GridRow>
                  <GridColumn>
                    Valve Status : {state.valve == 0 ? "ON" : "OFF"} <br />
                    <Button onClick={volvo}>
                      TURN {state.valve == 0 ? "OFF" : "ON"}
                    </Button>
                  </GridColumn>
                </GridRow>
                <GridRow centered columns={4} celled>
                  <GridColumn
                    style={{ textAlign: "center", fontSize: "1.5rem" }}
                  >
                    <Icon
                      name={
                        Math.floor(state.bate) > 75
                          ? "battery four"
                          : Math.floor(state.bate) > 50
                          ? "battery three"
                          : Math.floor(state.bate) > 30
                          ? "battery two"
                          : Math.floor(state.bate) > 10
                          ? "battery one"
                          : "battery zero"
                      }
                      color={
                        Math.floor(state.bate) > 50
                          ? "green"
                          : Math.floor(state.bate) > 30
                          ? "orange"
                          : "red"
                      }
                    />
                    {Math.floor(state.bate)}%
                  </GridColumn>
                </GridRow>
                <Grid.Row>
                  <Grid.Column>
                    <AreaChart
                      width={800}
                      height={250}
                      data={state.data}
                      margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
                    >
                      <defs>
                        <linearGradient
                          id="colorUv"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="1"
                        >
                          <stop
                            offset="5%"
                            stopColor="#8884d8"
                            stopOpacity={0.8}
                          />
                          <stop
                            offset="95%"
                            stopColor="#ffffff"
                            stopOpacity={0}
                          />
                        </linearGradient>
                      </defs>
                      <XAxis dataKey="waktu" />
                      <YAxis dataKey="debit" />
                      <Tooltip />
                      <Area
                        type="monotone"
                        dataKey="debit"
                        stroke="#8884d8"
                        fillOpacity={1}
                        fill="url(#colorUv)"
                      />
                    </AreaChart>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Card centered>
                      <Card.Content>
                        <Card.Header
                          textAlign="center"
                          content="Water Transfer Monthly"
                        />
                        <Card.Meta textAlign="center" content="in Liters" />
                        <Card.Description textAlign="center" />
                        {(
                          ((volume_bulanan + state.data_kumulatif) * 1.2) /
                          60
                        ).toFixed(3)}
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                  <Grid.Column>
                    <Card centered>
                      <Card.Content>
                        <Card.Header
                          textAlign="center"
                          content="Water Transfer Daily"
                        />
                        <Card.Meta textAlign="center" content="in Liters" />
                        <Card.Description textAlign="center" />
                        {(
                          ((volume_harian + state.data_kumulatif) * 1.2) /
                          60
                        ).toFixed(3)}
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
}

Dashboard.getInitialProps = async (ctx) => {
  let volume_bulanan = 0;
  let vale = 0;
  let volume_harian = 0;
  let pengurang_b = 0;
  let x = 0;
  let init_day = {};
  const payload = {};
  const header = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };
  await axios
    .get(API_BASE_URL + "volume/monthly/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        volume_bulanan = response.data.Volume;
      } else {
        console.log("eweuh");
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  await axios
    .get(API_BASE_URL + "volume/daily/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        volume_harian = response.data.Volume;
      } else {
        console.log("eweuh");
      }
    })
    .catch(function (error) {
      console.log(error);
    });

  await axios
    .get(API_BASE_URL + "volume/last/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        pengurang_b = response.data.Debit;
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  await axios
    .get(API_BASE_URL + "volume/day/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        init_day = response.data;
        init_day.pop();
      }
    })
    .catch(function (error) {
      console.log(error);
    });

  await axios
    .get(API_BASE_URL + "hardware/valve/pampam_01_01", payload, header)
    .then(function (response) {
      if (response.status === 200) {
        vale = response.data;
      }
    })
    .catch(function (error) {
      console.log(error);
    });

  return {
    volume_harian: volume_harian,
    volume_bulanan: volume_bulanan,
    pengurang_b: pengurang_b,
    init_day: init_day,
    x: x,
    vale: vale,
  };
};
