import React, { Component, useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  Grid,
  Segment,
  Menu,
  Dropdown,
  Container,
  Image,
  Input,
  Card,
  GridColumn,
  Form,
  CardHeader,
  Message,
} from "semantic-ui-react";

export const UserSettings = () => {
  // Router
  const router = useRouter();

  // State
  const [addressData, setAddressData] = useState({});
  const [newAddressData, setNewAddressData] = useState({});
  const [userData, setUserData] = useState({});
  const [newUserData, setNewUserData] = useState({});
  const [loader, setLoader] = useState({
    loading: false,
    success: true,
    error: true,
  });

  // Initial Fetch
  useEffect(() => {
    const fetchAdressData = async () => {
      try {
        const res = await fetch("http://3.133.109.15:8000/alamat/pampam_01_01");
        if (res.ok) {
          const data = await res.json();
          setAddressData(() => data[0]);
          setNewAddressData(() => data[0]);
        }
      } catch (err) {
        console.log(err);
      }
    };
    fetchAdressData();
  }, []);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const res = await fetch("http://3.133.109.15:8000/alamat/pampam_01_01");
        if (res.ok) {
          const data = await res.json();
        }
      } catch (err) {
        console.log(err);
      }
    };
  });

  // Functions
  const changeAddress = async () => {
    try {
      setLoader({ ...loader, loading: true });
      const res = await fetch(
        "http://3.133.109.15:8000/alamat/update/pampam_01_01",
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(newAddressData),
        }
      );

      if (res.ok) {
        setLoader({ ...loader, loading: false, success: false });
      }
    } catch (err) {
      setLoader({ ...loader, loading: false, error: false });
      console.log(err);
    }
  };

  return (
    <div>
      <Container>
        <Grid columns="equal">
          <Grid.Column>
            <Image
              style={{
                width: "70%",
                filter: "grayscale(100%)",
              }}
              src="/Logo_ds.png"
              rounded
            />
            <Menu secondary pointing vertical>
              <Link as="/dashboard/pampam_01_01" href="/dashboard/pampam_01_01">
                <Menu.Item name="Dashboard" active />
              </Link>
              <Menu.Item name="Analitics" />
              <Link href="/payment">
                <Menu.Item name="Payments" />
              </Link>
              <Dropdown item text="Settings">
                <Dropdown.Menu>
                  <Dropdown.Header>User Settings</Dropdown.Header>
                  <Dropdown.Item>Device</Dropdown.Item>
                  <Link href="/settings/user/pampam_01_01">
                    <Dropdown.Item>User</Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
          </Grid.Column>
          <Grid.Column
            width={12}
            style={{
              marginTop: "20px",
            }}
          >
            <Menu secondary>
              <Menu.Item name="home" />
              <Menu.Item name="messages" />
              <Menu.Item name="friends" />
              <Menu.Menu position="right">
                <Menu.Item>
                  <Input icon="search" placeholder="Search..." />
                </Menu.Item>
                <Menu.Item name="logout" />
              </Menu.Menu>
            </Menu>
            <Segment padded="medium">
              <Grid columns="equal">
                <Grid.Row>
                  <Grid.Column>User Settings and Data</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column></Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Form onSubmit={changeAddress} loading={loader.loading}>
                      <Message
                        success={loader.success}
                        header="Form Completed"
                        content="Your data is saved successfully"
                      />
                      <Message
                        success={loader.error}
                        header="Whoops!"
                        content="Something went wrong while submitting..."
                      />
                      <Form.Group widths="equal">
                        <Form.Input
                          fluid
                          label="Jalan"
                          placeholder={addressData.jalan}
                          value={newAddressData.jalan}
                          onChange={(e) =>
                            setNewAddressData({
                              ...newAddressData,
                              jalan: e.target.value,
                            })
                          }
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Input
                          fluid
                          label="Koordinat"
                          placeholder={addressData.koordinat}
                          value={newAddressData.koordinat}
                          onChange={(e) =>
                            setNewAddressData({
                              ...newAddressData,
                              koordinat: e.target.value,
                            })
                          }
                        />
                      </Form.Group>
                      <Form.Group widths="equal">
                        <Form.Input
                          fluid
                          label="Kelurahan"
                          placeholder={addressData.kelurahan}
                          value={newAddressData.kelurahan}
                          onChange={(e) =>
                            setNewAddressData({
                              ...newAddressData,
                              kelurahan: e.target.value,
                            })
                          }
                        />
                        <Form.Input
                          fluid
                          label="Kecamatan"
                          value={newAddressData.kecamatan}
                          onChange={(e) =>
                            setNewAddressData({
                              ...newAddressData,
                              kecamatan: e.target.value,
                            })
                          }
                        />
                        <Form.Input
                          fluid
                          label="Kabupaten"
                          value={newAddressData.kabupaten}
                          onChange={(e) =>
                            setNewAddressData({
                              ...newAddressData,
                              kabupaten: e.target.value,
                            })
                          }
                        />
                      </Form.Group>
                      <Form.Button>Update</Form.Button>
                    </Form>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
};
