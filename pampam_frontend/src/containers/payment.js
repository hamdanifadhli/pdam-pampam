import {
  Container,
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
  Menu,
  Dropdown,
  Input,
  Card,
  Label,
  Table,
  Icon,
} from "semantic-ui-react";
import Link from "next/link";
import { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { API_BASE_URL, MERGE_ID, USER_ID } from "../api/default";

export function Payment() {
  const [paymentState, setPaymentState] = useState({
    devices: [],
  });

  useEffect(() => {
    const inititalFetch = async () => {
      try {
        const res = await fetch(`${API_BASE_URL}api/device`, {
          "Access-Control-Allow-Origin": "*",
        });
        if (res.ok) {
          let data = await res.json();

          for (let i = 0; i < data.length; i++) {
            try {
              const res = await fetch(
                `${API_BASE_URL}volume/monthly/${data[i].merge_id}`,
                {
                  "Access-Control-Allow-Origin": "*",
                }
              );

              if (res.ok) {
                const peh = await res.json();

                data[i] = { ...data[i], volume: peh.Volume };
              }
            } catch (err) {
              console.log(err);
            }
          }
          setPaymentState({ devices: data });
        }
      } catch (err) {
        console.log(err);
      }
    };
    inititalFetch();
  }, []);

  const formatMoney = (
    amount,
    decimalCount = 2,
    decimal = ",",
    thousands = "."
  ) => {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount))
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : "")
      );
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div>
      <Container>
        <Grid columns="equal">
          <Grid.Column>
            <Image
              style={{
                width: "70%",
                filter: "grayscale(100%)",
              }}
              src="/Logo_ds.png"
              rounded
            />
            <Menu secondary pointing vertical>
              <Link as="/dashboard/pampam_01_01" href="/dashboard/pampam_01_01">
                <Menu.Item name="Dashboard" active />
              </Link>
              <Menu.Item name="Analitics" />
              <Dropdown item text="Settings">
                <Dropdown.Menu>
                  <Dropdown.Header>User Settings</Dropdown.Header>
                  <Dropdown.Item>Device</Dropdown.Item>
                  <Link href="/settings/user/pampam_01_01">
                    <Dropdown.Item>User</Dropdown.Item>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
          </Grid.Column>
          <Grid.Column
            width={12}
            style={{
              marginTop: "20px",
            }}
          >
            <Menu secondary>
              <Menu.Item name="home" />
              <Menu.Item name="messages" />
              <Menu.Item name="friends" />
              <Menu.Menu position="right">
                <Menu.Item>
                  <Input icon="search" placeholder="Search..." />
                </Menu.Item>
                <Menu.Item name="logout" />
              </Menu.Menu>
            </Menu>
            <Grid columns="equal">
              <Grid.Row>
                <Grid.Column>
                  My Devices
                </Grid.Column>
              </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Input
                    fluid
                      size="huge"
                      icon={{ name: "search", circular: true, link: true }}
                      placeholder="Search..."
                    />
                  </Grid.Column>
                </Grid.Row>
              <Grid.Row>
                <Grid.Column>
                  <Card.Group itemsPerRow={3}>
                    <Table celled>
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>No. </Table.HeaderCell>
                          <Table.HeaderCell>Device</Table.HeaderCell>
                          <Table.HeaderCell>Monthly Volume</Table.HeaderCell>
                          <Table.HeaderCell>Total Price</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        {paymentState.devices.map((device, index) => {
                          return (
                            <Table.Row key={index}>
                              <Table.Cell>{index + 1}</Table.Cell>
                              <Table.Cell>{device.merge_id}</Table.Cell>
                              <Table.Cell>
                                {formatMoney(
                                  Math.floor((device.volume * 100) / 60) / 100
                                )}{" "}
                                L
                              </Table.Cell>
                              <Table.Cell>
                                Rp{" "}
                                {formatMoney(
                                  (Math.floor((device.volume * 100) / 60) /
                                    100) *
                                    7450 +
                                    7400
                                )}
                              </Table.Cell>
                            </Table.Row>
                          );
                        })}
                      </Table.Body>
                    </Table>
                  </Card.Group>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
}
