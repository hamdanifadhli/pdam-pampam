import { useRouter } from "next/router";
import {
  Grid,
  Segment,
  Menu,
  Dropdown,
  Container,
  Image,
  Input,
  Card,
} from "semantic-ui-react";
import { useState } from "react";
import axios from "axios";
import { AreaChart, Area, Tooltip, XAxis, YAxis } from "recharts";
import { API_BASE_URL, MERGE_ID, USER_ID } from "../../src/api/default";

export default function Dashboard() {
  let x = 0;
  const router = useRouter();
  const data = [
    {
      name: "Page A",
      uv: 4000,
      pv: 2400,
    },
    {
      name: "Page B",
      uv: 3000,
      pv: 1398,
    },
    {
      name: "Page C",
      uv: 2000,
      pv: 9800,
    },
    {
      name: "Page D",
      uv: 2780,
      pv: 3908,
    },
    {
      name: "Page E",
      uv: 1890,
      pv: 4800,
    },
    {
      name: "Page F",
      uv: 2390,
      pv: 3800,
    },
    {
      name: "Page G",
      uv: 3490,
      pv: 4300,
    },
  ];
  const [state, setState] = useState({
    volume_bulanan: "",
    volume_harian: "",
    data: "",
  });

  const payload = {};
  const header = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };
  if (x == 0) {
    axios.get(API_BASE_URL + "volume", payload, header).then(function (r) {
      if (r.status == 200) {
        console.log(r.data);
      }
    });
    x = 1;
  }

  return (
    <div>
      <Container>
        <Grid columns="equal">
          <Grid.Column>
            <Image
              style={{
                width: "70%",
                filter: "grayscale(100%)",
              }}
              src="/Logo_ds.png"
              rounded
            />
            <Menu secondary pointing vertical>
              <Menu.Item name="Dashboard" active />
              <Menu.Item name="Analitics" />
              <Dropdown item text="Settings">
                <Dropdown.Menu>
                  <Dropdown.Header>User Settings</Dropdown.Header>
                  <Dropdown.Item>Device</Dropdown.Item>
                  <Dropdown.Item>User</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu>
          </Grid.Column>
          <Grid.Column
            width={12}
            style={{
              marginTop: "20px",
            }}
          >
            <Menu secondary>
              <Menu.Item name="home" />
              <Menu.Item name="messages" />
              <Menu.Item name="friends" />
              <Menu.Menu position="right">
                <Menu.Item>
                  <Input icon="search" placeholder="Search..." />
                </Menu.Item>
                <Menu.Item name="logout" />
              </Menu.Menu>
            </Menu>
            <Segment padded="medium">
              <Grid columns="equal">
                <Grid.Row>
                  <Grid.Column>Welcome {router.query.id}</Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <AreaChart
                      width={800}
                      height={250}
                      data={data}
                      margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
                    >
                      <defs>
                        <linearGradient
                          id="colorUv"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="1"
                        >
                          <stop
                            offset="5%"
                            stopColor="#8884d8"
                            stopOpacity={0.8}
                          />
                          <stop
                            offset="95%"
                            stopColor="#8884d8"
                            stopOpacity={0}
                          />
                        </linearGradient>
                        <linearGradient
                          id="colorPv"
                          x1="0"
                          y1="0"
                          x2="0"
                          y2="1"
                        >
                          <stop
                            offset="5%"
                            stopColor="#82ca9d"
                            stopOpacity={0.8}
                          />
                          <stop
                            offset="95%"
                            stopColor="#82ca9d"
                            stopOpacity={0}
                          />
                        </linearGradient>
                      </defs>
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Area
                        type="monotone"
                        dataKey="uv"
                        stroke="#8884d8"
                        fillOpacity={1}
                        fill="url(#colorUv)"
                      />
                      <Area
                        type="monotone"
                        dataKey="pv"
                        stroke="#82ca9d"
                        fillOpacity={1}
                        fill="url(#colorPv)"
                      />
                    </AreaChart>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Card centered>
                      <Card.Content>
                        <Card.Header
                          textAlign="center"
                          content="Water Transfer Monthly"
                        />
                        <Card.Meta textAlign="center" content="in Liters" />
                        <Card.Description textAlign="center" />
                        {state.volume_bulanan / 60} Liter
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                  <Grid.Column>
                    <Card centered>
                      <Card.Content>
                        <Card.Header
                          textAlign="center"
                          content="Water Transfer Daily"
                        />
                        <Card.Meta textAlign="center" content="in Liters" />
                        <Card.Description textAlign="center" />
                        {state.volume_harian / 60} Liter
                      </Card.Content>
                    </Card>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
}
