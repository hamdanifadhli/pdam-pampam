module pampam/backend/tuqa

go 1.15

require (
	github.com/ambelovsky/gosf-socketio v0.0.0-20200621194023-a8dfd749dd6f
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/googollee/go-socket.io v1.4.4
	github.com/jinzhu/gorm v1.9.16
	github.com/tpkeeper/gin-dump v1.0.0
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
)
